from django.urls import path
from accounts.views import user_login, user_logout, user_signup

urlpatterns = [
    path("login/", user_login, name="login"),
    # Feat 9
    path("logout/", user_logout, name="logout"),
    # Feat 10
    path("signup/", user_signup, name="signup"),
]

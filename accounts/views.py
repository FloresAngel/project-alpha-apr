from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


# Create your views here.
# Feat 7
def user_login(request):
    if request.method == "POST":
        formlogin = LoginForm(request.POST)
        if formlogin.is_valid():
            username = formlogin.cleaned_data["username"]
            password = formlogin.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        formlogin = LoginForm()
    context = {
        "login_form": formlogin,
    }
    return render(request, "accounts/login.html", context)


# Feat 9
def user_logout(request):
    logout(request)
    return redirect("login")


# Feat 10
def user_signup(request):
    if request.method == "POST":
        signup = SignupForm(request.POST)
        if signup.is_valid():
            username = signup.cleaned_data["username"]
            password = signup.cleaned_data["password"]
            password_confirmation = signup.cleaned_data[
                "password_confirmation"
            ]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )

                login(request, user)

                return redirect("list_projects")
            else:
                signup.add_error("password", "The passwords do not match")
    else:
        signup = SignupForm()
    context = {
        "signupform": signup,
    }
    return render(request, "accounts/signup.html", context)

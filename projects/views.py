from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    list_proj = Project.objects.filter(owner=request.user)
    context = {
        "proj_list": list_proj,
    }
    return render(request, "projects/list.html", context)


# Feat 13
@login_required
def detail_projects(request, id):
    detail_proj = get_object_or_404(Project, id=id)
    context = {
        "details_projs": detail_proj,
    }
    return render(request, "projects/detail.html", context)


# Feat 14
@login_required
def create_project(request):
    if request.method == "POST":
        create_proj = ProjectForm(request.POST)
        if create_proj.is_valid():
            create_proj.save()
            return redirect("list_projects")
    else:
        create_proj = ProjectForm()
    context = {
        "createproject": create_proj,
    }
    return render(request, "projects/createproject.html", context)

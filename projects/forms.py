from django.forms import ModelForm
from projects.models import Project


# Feat 14
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]

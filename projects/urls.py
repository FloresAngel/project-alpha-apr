from django.urls import path
from projects.views import (
    list_projects,
    detail_projects,
    create_project,
)

urlpatterns = [
    path("", list_projects, name="list_projects"),
    # Feat 13
    path("<int:id>/", detail_projects, name="show_project"),
    # Feat 14
    path("create/", create_project, name="create_project"),
]

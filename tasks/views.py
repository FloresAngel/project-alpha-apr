from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task

# Create your views here.


# Feat 15
@login_required
def create_task(request):
    if request.method == "POST":
        create_tsk = TaskForm(request.POST)
        if create_tsk.is_valid():
            create_tsk.save()
            return redirect("list_projects")
    else:
        create_tsk = TaskForm()
    context = {"createtask": create_tsk}
    return render(request, "tasks/createtask.html", context)


# Feat #16
@login_required
def list_task(request):
    mytasks = Task.objects.filter(assignee=request.user)
    context = {"mylisttasks": mytasks}
    return render(request, "tasks/mytask.html", context)

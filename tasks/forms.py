from django import forms
from tasks.models import Task


# Feat 15
class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]

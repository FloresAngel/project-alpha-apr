from django.urls import path
from tasks.views import create_task, list_task

# Feat 15
urlpatterns = [
    path("create/", create_task, name="create_task"),
    # Feat 16
    path("mine/", list_task, name="show_my_tasks"),
]

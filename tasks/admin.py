from django.contrib import admin
from tasks.models import Task

# Register your models here.


# Feat 12
@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )
